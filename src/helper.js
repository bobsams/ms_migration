exports.nameCleaner = name => name.toLowerCase().replace(/[^a-z0-9 ]/g, "")

exports.titleCase = str => str.replace(/\b\S/g, t => t.toUpperCase());

exports.hasSkuDetail = x => x.sku_detail.length > 0
exports.hasSupplierCatalogDetails = x => x.supplier_catalog_details.length > 0

exports.hasSkuDetailAndSupplierCatalog = x => x.sku_detail && x.supplier_catalog_details