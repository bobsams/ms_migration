const originDB = require('../database/origin/models')
const destinationDB = require('../database/destination/models')
const {Op} = require('sequelize')
const _ = require('lodash')

/**
* Product, 
ProductSupplier,
ProductReseller,
ProductSku,
ProductSkuSupplier,
ProductSkuReseller,
ProductImage,
ProductStat,
OptionType,
OptionValue
_________________________
DESTINATION DB
*/

/**
* Product,
* ProductOption
* ProductOptionValue
* ProductVariant
* ProductSku
* ProductImage
* SupplierCatalog
* TokoCatalog
  MigrationProductLog
*/


module.exports = {
  async getAllData (limit = 1000, lastID) {
    const [products, types, values, maxPsku, maxPskuSupplier] = await Promise.all([
      originDB.Product.findAll({
        where: {
          supplier_id: {[ Op.not ]: null},
          toko_id: {[Op.is]: null},
          id: {[Op.gt]: lastID},
          status: {[Op.not]: 'deleted'},
          is_tokotalk: {
            [Op.or]: [false, {[Op.is]: null}]
          }
        },
        include: [
          {
            model: originDB.ProductOptions,
            as: 'options',
            attributes: ['id','name'],
            include: [
              {
                model: originDB.ProductOptionValues,
                as: 'optionValues',
                attributes: ['id','name'],
              }
            ]
          },
          {
            model: originDB.ProductVariants,
            as: 'variants',
            attributes: ['id', 'product_sku_id'],
            include: [
              {
                model: originDB.ProductSKU,
                as: 'sku_detail',
                attributes: ['stock', 'price', 'sku', 'code']
              },
              {
                model: originDB.SupplierCatalog,
                as: 'supplier_catalog_details',
                attributes: ['markup', 'status', 'minimum_reseller_markup']
              }
            ]
          }
        ],
        order: [['id','ASC']],
        limit
      }),
      destinationDB.OptionType.findAll({ attributes: ['id', 'name'] }),
      destinationDB.OptionValue.findAll({ attributes: ['id', 'name']}),
      destinationDB.ProductSku.max('id'),
      destinationDB.ProductSkuSupplier.max('id')
    ])
    return { 
      products: JSON.parse(JSON.stringify(products)), 
      types: JSON.parse(JSON.stringify(types)), 
      values: JSON.parse(JSON.stringify(values)),
      maxPsku, maxPskuSupplier
    }
  },
  typeMap (types) {
    return {
      dictionary: _(types).groupBy(x => x.name).value(),
      maxID: _.max(types.map(x => x.id))
    }
  },
  valueMap(values) {
    return {
      dictionary: _(values).groupBy(x => x.name).value(),
      maxID: +_.max(values.map(x => x.id))
    }
  },
  productDestructure (productData) {
    const { 
      id, safe_url, name, category_id, price, weight, description, 
      toko_id, supplier_id, source_url,
      capital_price, QC_passed, status, quality, video_url, categories,
      is_tokotalk, total_view, total_share, view_booster, createdAt, updatedAt,
      options, variants
    } = productData
    
    const [category1, category2, category3] = categories.split(',')
    return {
      product: {
        id,
        name,
        safe_url,
        description,
        weight,
        video_url,
        quality,
        QC_passed,
        status,
        category1,
        category2,
        category3,
        created_at: createdAt,
        updated_at: updatedAt
      },
      productStat: {
        view: total_view,
        share: total_share
      },
      productReseller: {
        product_id: id,
        toko_id
      },
      productSupplier: {
        product_id: id,
        supplier_id
      },
      options,
      variants
    }
  },
  pskuMap(variants, optionValueDictionary) {
    // optionValueDictionary = { oldPOV: newOptionValue }
    const mappedPSKU = _(variants).groupBy('product_sku_id').map((value, key) => {
      const { sku_detail: { stock, price, sku, code }, supplier_catalog_details: { markup, status, minimum_reseller_markup }} = value[0]
      let [oldVal1, oldVal2, oldVal3 ] = code.split(':')[2].split('.')
      const opt_val_id_1 = optionValueDictionary[oldVal1] || undefined
      const opt_val_id_2 = optionValueDictionary[oldVal2] || undefined
      const opt_val_id_3 = optionValueDictionary[oldVal3] || undefined
      return {
        pSku: { stock, price, sku, opt_val_id_1, opt_val_id_2, opt_val_id_3 },
        pSkuSupplier: { markup, status, minimum_reseller_markup },
        marketplacePrice: price + markup
      }
    }).value()
    return mappedPSKU
  },
  async migrate(allData) {
    const transaction = await destinationDB.sequelize.transaction()
    try {
      const {
        finalProducts,
        finalProductSupplier,
        finalProductReseller,
        finalOptionType,
        finalOptionValue,
        finalPsku,
        finalPskuSupplier,
        finalProductStat,
        productMigrationLogs
      } = allData

      await Promise.all([
        destinationDB.Product.bulkCreate(finalProducts, { transaction }), // no id
        destinationDB.ProductSupplier.bulkCreate(finalProductSupplier, { transaction }), // no id
        destinationDB.ProductReseller.bulkCreate(finalProductReseller, { transaction }), // no id
        destinationDB.OptionType.bulkCreate(finalOptionType, { transaction }), // with id
        destinationDB.OptionValue.bulkCreate(finalOptionValue, { transaction }), // with id
        destinationDB.ProductSku.bulkCreate(finalPsku, { transaction }), // with id
        destinationDB.ProductSkuSupplier.bulkCreate(finalPskuSupplier, { transaction }), // with id
        destinationDB.ProductStat.bulkCreate(finalProductStat, { transaction }), // no id
        destinationDB.MigrationProductLog.bulkCreate(productMigrationLogs, { transaction })
      ])

      await transaction.commit()
    }
    catch(err) {
      console.log(err)
      await transaction.rollback()
      throw err
    }
  },

  async getLastID() {
    try {
      const lastID = await destinationDB.MigrationProductLog.max('product_id')
      return lastID || 0
    }
    catch(err) {
     throw err
    }
  }
}