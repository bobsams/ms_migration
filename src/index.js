const originDB = require('../database/origin/models')
const destinationDB = require('../database/destination/models')
const sequelize = require('sequelize')

/**
 * Product, 
  ProductSupplier,
  ProductReseller,
  ProductSku,
  ProductSkuSupplier,
  ProductSkuReseller,
  ProductImage,
  ProductStat,
  OptionType,
  OptionValue
_________________________
  DESTINATION DB
 */

  /**
   * Product,
   * ProductOption
   * ProductOptionValue
   * ProductVariant
   * ProductSku
   * ProductImage
   * SupplierCatalog
   * TokoCatalog
   */


const nameCleaner = name => name.toLowerCase().replace(/[^a-z0-9 ]/g, "")
const titleCase = str => str.replace(/\b\S/g, t => t.toUpperCase());

module.exports = {
  product: productData => {
    const { 
      id, safe_url, name, category_id, price, weight, description, 
      toko_id, supplier_id, source_url,
      capital_price, QC_passed, status, quality, video_url, categories,
      is_tokotalk, total_view, total_share, view_booster, createdAt, updatedAt
    } = productData
    
    const [category1, category2, category3] = categories.split(',')
    return {
      product: {
        id,
        name,
        safe_url,
        description,
        weight,
        video_url,
        quality,
        QC_passed,
        status,
        category1,
        category2,
        category3,
        created_at: createdAt,
        updated_at: updatedAt
      },
      productStat: {
        view: total_view,
        share: total_share
      },
      productReseller: {
        product_id: id,
        toko_id
      },
      productSupplier: {
        product_id: id,
        supplier_id
      }
    }
  },
  options: async (PO, POV, transaction) => {
    try {
      let finalOptions = []

      for (let i = 0; i < PO.length; i++) {
        const { id: poID, name: poName } = PO[i]
        let PO_TEMP = {}
        PO_TEMP['before'] =  { id: poID, name: poName }
        
        // check if po registed. if not, create one
        const cleanedPOName = titleCase(nameCleaner(poName))
        const PONameExist = await OptionType.findOne({
          where: {
            name: cleanedPOName
          }
        })
        
        if (PONameExist) {
          PO_TEMP['after'] = { id: PONameExist.id, name: PONameExist.name }
        } else {
          const newOptionType = await OptionType.create({
            name: cleanedPOName,
            master_data: false
          }, { returning: true, transaction })
          PO_TEMP['after'] = { id: newOptionType.id, name: newOptionType.name }
        }
        
        // get its POVs
        const povs = POV.filter( x => x.product_option_id === poID)
        const mappedPovs = await Promise.all(povs.map( async x => {
          const { id: povId, name: povName, product_option_id } = x
          let POV_TEMP = { before: { id: povId, name: povName, product_option_id }}

          // check if pov/opt_values exist
          const cleanedPOVName = titleCase(nameCleaner(povName))
          const POVNameExist = await OptionValue.findOne({
            where: {
              name: cleanedPOVName
            }
          })
          
          if (POVNameExist) {
            // console.log(`${cleanedPOVName} exist in database`)
            POV_TEMP['after'] = { id: POVNameExist.id, name: POVNameExist.name }
          } else {
            // console.log(`${cleanedPOVName} not exist in database`)
            const newOptionValue = await OptionValue.create({
              opt_type_id: PO_TEMP['after'].id,
              name: cleanedPOVName,
              master_data: false
            }, { returning: true, transaction })
            POV_TEMP['after'] = { id: newOptionValue.id, name: newOptionValue.name, opt_type_id: PO_TEMP['after'].id }
          }
          return POV_TEMP
        }))
        finalOptions.push({ optType: PO_TEMP, optVal: mappedPovs })
      }
      return finalOptions
    }
    catch(err) {
      throw err
    }
  },
  psku: async (PSKU, finalOptions, product_id, transaction) => {
    try {
      const optValDict = {}
      const hasVariants = finalOptions.length > 0

      for (let options of finalOptions) {
        for (let val of options.optVal) {
          const { before } = val
          if (!(before.id in optValDict)) optValDict[before.id] = val
        }
      }

      const newSku = await Promise.all(PSKU.map( async _psku => {
        const { id, sku, stock, price, code } = _psku
        let finalPsku = {
          product_id,
          sku,
          price,
          stock,
        }
        if (hasVariants) {
          const valuesFromCode = code.split(':')[2].split('.')
          const [opt_val_id_1, opt_val_id_2, opt_val_id_3] = valuesFromCode.map(val => optValDict[val].after.id)  
          finalPsku = Object.assign(finalPsku, { opt_val_id_1, opt_val_id_2, opt_val_id_3 })
        }
        const createdSkuData = await ProductSku.create(finalPsku, { transaction })
        return {
          before: id,
          after: JSON.parse(JSON.stringify(createdSkuData))
        }
      }))
      
      const finalSKU = newSku.reduce((acc, curr) => {
        const { before: id, after } = curr
        if (!(id in acc)) acc[id] = after
        return acc
      }, {})
      
      return finalSKU
    }
    catch(err) {
      throw err
    }
  },
  skuSupplier: (PV, S_CATALOG, finalSKU, _productStat, product_id) => {
    try {
      // unique pvID per skuID.
      let visitedSKU = {}, uniquePV = {} // { pv: psku }
      for (let pv of PV) {
        if (!(pv.product_sku_id in visitedSKU)) {
          visitedSKU[pv.product_sku_id] = true
          uniquePV[pv.id] = pv.product_sku_id
        }
      }

      // filter supplier catalog by uniquePV
      const uniqueSuppCatalog = S_CATALOG.filter(sCat => sCat.product_variant_id in uniquePV)

      // join uniqueSuppCatalog with finalSKU. Keep track of min and max price
      let minPrice = Infinity, maxPrice = 0
      const finalSupplierSKU = uniqueSuppCatalog.map(sCat => {
        const { product_variant_id, supplier_id, markup, minimum_reseller_markup, status } = sCat
        const { id: newSkuId, price } = finalSKU[uniquePV[product_variant_id]]

        const marketplacePrice = +price + +markup
        minPrice = marketplacePrice < minPrice ? marketplacePrice : minPrice
        maxPrice = marketplacePrice > maxPrice ? marketplacePrice : maxPrice

        return {
          sku_id: newSkuId,
          supplier_id,
          markup,
          min_resell_markup: minimum_reseller_markup,
          is_active: status
        }
      })
      const finalProductStat = { product_id, min_price: minPrice, max_price: maxPrice, ..._productStat }
      return { finalSupplierSKU, finalProductStat }
    }
    catch(err) {
      throw err
    }
  }
}