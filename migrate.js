console.log('migration starts')

const fs = require('fs')
const path = require('path')
const { Console } = require('console')
// var accessLogStream = fs.createWriteStream(path.join(__dirname, 'migration_success.log'), { flags: 'a' });

const Logger = new Console({
  stdout: fs.createWriteStream(path.join(__dirname, 'FINAL2_migration_success.log'), { flags: 'a' }),
  stderr: fs.createWriteStream(path.join(__dirname, 'FINAL2_migration_failed.log'), { flags: 'a' })
})

const { getAllData, typeMap, valueMap, productDestructure, pskuMap, migrate, getLastID } = require('./src/main')
const { nameCleaner, titleCase, hasSkuDetailAndSupplierCatalog } = require('./src/helper')
const { max, min } = require('lodash')
// const totalProduct = 10000


async function run () {
  let _lastProductId
  try {
    const t0 = new Date()
    const lastProductId = await getLastID()
    const totalProduct = process.argv[2] || 10000
    console.log(`migrating ${totalProduct} products. last productID: ${lastProductId}`)

    let { products, types, values, maxPsku, maxPskuSupplier } = await getAllData(totalProduct, lastProductId)
    maxPsku = maxPsku || 0
    maxPskuSupplier = maxPskuSupplier || 0
    let { dictionary: typeDict, maxID: typeMaxID } = typeMap(types)
    let { dictionary: valueDict, maxID: valueMaxID } = valueMap(values)
    // typeDict: {id: [{id,name}]} valueDict: {id: [{id,name}]}

    let finalProducts         = [], 
        finalProductSupplier  = [], 
        finalProductReseller  = [],
        finalOptionType       = [],
        finalOptionValue      = [],
        finalPsku             = [],
        finalPskuSupplier     = [],
        finalProductStat      = [],
        productMigrationLogs  = []

    for(let _product of products) {
      console.log(`processing product ID ${_product.id}`)

      const { product, productReseller, productSupplier, productStat, options, variants} = productDestructure(_product)
      _lastProductId = product.id
      if (!variants.every(hasSkuDetailAndSupplierCatalog)) {
        Logger.error(`${_lastProductId} ::: does not have sku_detail or supplier catalog ::: ${new Date().toISOString()}`)
        productMigrationLogs.push({ product_id: _lastProductId, status: false })
        continue
      }

      // options: [{id, name, optionValues: [{id, name}]}]
      // variants: [{id, sku_details: {'stock', 'price', 'sku', 'code'}, supplier_catalog_details:{} }]

      finalProducts.push(product)
      if (productSupplier.supplier_id) finalProductSupplier.push(productSupplier)
      if (productReseller.toko_id) finalProductReseller.push(productReseller)

      const optionValueDictionary = {}
      // optionValueDictionary = {oldPOV: newOptionValue}
      for (let { name: optName, optionValues} of options) {
        // id, name, optionValues: [{ name }]
        const cleanedOptName = titleCase(nameCleaner(optName))
        let optTypeID
        if (!(cleanedOptName in typeDict)) {
          // product option not in optionType
          typeMaxID += 1
          optTypeID = typeMaxID
          finalOptionType.push({ id: optTypeID, name: cleanedOptName, master_data: false })
          typeDict[cleanedOptName] = [{ id: typeMaxID, name: cleanedOptName }]
        } else {
          optTypeID = typeDict[cleanedOptName][0]['id']
        }

        for (let { id: oldPOV, name: valueName } of optionValues) {
          const cleanedValName = titleCase(nameCleaner(valueName))
          let optValID
          if (!(cleanedValName in valueDict)) {
            valueMaxID += 1
            optValID = valueMaxID
            finalOptionValue.push({ id: optValID, opt_type_id: optTypeID, name: cleanedValName, master_data: false })
            valueDict[cleanedValName] = [{ id: valueMaxID, name:cleanedValName }]
          } else {
            optValID = valueDict[cleanedValName][0]['id']
          }
          optionValueDictionary[oldPOV] = optValID
        }
      }

      const mappedPSKU = pskuMap(variants, optionValueDictionary, productStat)
      const prices = []
      for( let newSKU of mappedPSKU ) {
        const { pSku, pSkuSupplier, marketplacePrice } = newSKU
        prices.push(marketplacePrice)
        finalPsku.push(Object.assign(pSku, { id: maxPsku + 1, product_id: product.id }))
        finalPskuSupplier.push(Object.assign(pSkuSupplier, { id: maxPskuSupplier + 1, sku_id: maxPsku + 1 }))
        maxPsku++
        maxPskuSupplier++
      }
      const newProductStat = Object.assign(productStat, { product_id: product.id, max_price: max(prices), min_price: min(prices)})
      finalProductStat.push(newProductStat)
      Logger.log(`${_lastProductId} ::: ${new Date().toISOString()}`)
      productMigrationLogs.push({ product_id: _lastProductId, status: true })
    }

    // console.log(finalPsku)
    if (finalProducts.length > 0) {
      await migrate({
        finalProducts,
        finalProductSupplier,
        finalProductReseller,
        finalOptionType,
        finalOptionValue,
        finalPsku,
        finalPskuSupplier,
        finalProductStat,
        productMigrationLogs
      })  
    }
    
    console.log(`migration done. last productID: ${_lastProductId}.  elapsed time: ${new Date() - t0}`)
    return true
  }
  catch(e) {
    Logger.error(_lastProductId)
    console.log(`error when proccessing product ID ${_lastProductId}`)
    throw e
  }
}

run()
.then(x => {
  console.log(x)
  process.exit(0)
})
.catch(e => {
  console.log(e)
})