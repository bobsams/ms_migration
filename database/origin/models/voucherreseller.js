'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class VoucherReseller extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      VoucherReseller.belongsTo(models.Voucher, {
        foreignKey: 'voucher_id',
        as: 'voucher'
      });
    }
  };
  VoucherReseller.init({
    voucher_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'VoucherReseller',
  });
  return VoucherReseller;
};