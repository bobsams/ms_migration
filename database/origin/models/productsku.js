'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductSKU extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      ProductSKU.hasMany(models.ProductVariants, {
        foreignKey: 'product_sku_id',
        as: 'variants'
      });
    }
  };
  ProductSKU.init({
    code: DataTypes.STRING,
    sku: DataTypes.STRING,
    price: DataTypes.INTEGER,
    stock: DataTypes.INTEGER,
    minimum_stock: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'ProductSKU',
  });
  return ProductSKU;
};