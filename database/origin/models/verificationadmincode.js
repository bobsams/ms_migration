'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class VerificationAdminCode extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  VerificationAdminCode.init({
    admin_user_id: DataTypes.INTEGER,
    token: DataTypes.STRING,
    expire: DataTypes.DATE,
    source: DataTypes.STRING,
    data: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'VerificationAdminCode',
  });
  return VerificationAdminCode;
};