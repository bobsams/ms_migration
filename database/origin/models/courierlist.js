'use strict';
module.exports = (sequelize, DataTypes) => {
  const CourierList = sequelize.define('CourierList', {
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    image: {
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING
    },
    cod_support: {
      allowNull: false,
      type: DataTypes.BOOLEAN
    },
    cod_rate_percent: {
      type: DataTypes.DECIMAL
    },
    cod_rate_flat: {
      type: DataTypes.DECIMAL
    },
  }, {});
  CourierList.associate = function(models) {
    // associations can be defined here
    CourierList.hasMany(models.CourierListService, {
      foreignKey: 'courier_id',
      as: 'services'
    })
  };
  return CourierList;
};