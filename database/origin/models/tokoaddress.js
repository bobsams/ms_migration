'use strict';
module.exports = (sequelize, DataTypes) => {
  const TokoAddress = sequelize.define('TokoAddress', {
    toko_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    country: {
      allowNull: false,
      type: DataTypes.STRING
    },
    province_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    city_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    district_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    postal_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address: {
      allowNull: false,
      type: DataTypes.STRING
    },
    address2: {
      type: DataTypes.STRING
    },
  }, {});
  TokoAddress.associate = function(models) {
    // associations can be defined here
    TokoAddress.belongsTo(models.Province, {
      foreignKey: 'province_id',
      as: 'province_details'
    });
    TokoAddress.belongsTo(models.City, {
      foreignKey: 'city_id',
      as: 'city_details'
    })
    TokoAddress.belongsTo(models.Toko, {
      foreignKey: 'toko_id',
      as: 'addresses'
    })
    TokoAddress.belongsTo(models.District, {
      foreignKey: 'district_id',
      as: 'district_details'
    });
  };
  return TokoAddress;
};