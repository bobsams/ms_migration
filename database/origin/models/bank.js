'use strict';
module.exports = (sequelize, DataTypes) => {
  const Bank = sequelize.define('Bank', {
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    valid_lengths: {
      type: DataTypes.STRING
    },
    admin_fee: {
      type: DataTypes.INTEGER
    },
    logo: {
      type: DataTypes.STRING
    },
    priority: {
      type: DataTypes.INTEGER
    },
    minimum_withdrawal: {
      type: DataTypes.INTEGER
    },
  }, {});
  Bank.associate = function(models) {
    // associations can be defined here
  };
  return Bank;
};