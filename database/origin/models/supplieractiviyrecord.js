'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class SupplierActivityRecord extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  SupplierActivityRecord.init({
    activity: DataTypes.STRING,
    activity_detail: DataTypes.STRING,
    supplier_id: DataTypes.INTEGER,
    product_id: DataTypes.INTEGER,
    user_role: DataTypes.STRING,
    user_name: DataTypes.STRING,
    req_payload: DataTypes.TEXT,
    res_payload: DataTypes.TEXT,
    success: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'SupplierActivityRecord',
  });
  return SupplierActivityRecord;
};