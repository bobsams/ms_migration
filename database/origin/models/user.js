'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    phone: {
      type: DataTypes.STRING,
    },
    name: {
      type: DataTypes.STRING,
    },
    photo: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    pin: {
      type: DataTypes.STRING
    },
    reg_id: {
      type: DataTypes.STRING
    },
    is_phone_verified: {
      type: DataTypes.BOOLEAN
    },
  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.hasOne(models.Toko, {
      foreignKey: 'user_id',
      as: 'toko'
    });
    User.hasOne(models.UserKYC, {
      foreignKey: 'user_id',
      as: 'kyc_details'
    });
  };
  return User;
};