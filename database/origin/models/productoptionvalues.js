'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductOptionValues extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ProductOptionValues.belongsTo(models.ProductOptions, {
        foreignKey: 'product_option_id',
      });
      ProductOptionValues.hasMany(models.ProductVariants,{
        foreignKey: 'product_option_value_id',
        as: 'option'

      })
    }
  };
  ProductOptionValues.init({
    product_option_id: DataTypes.INTEGER,
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProductOptionValues',
  });
  return ProductOptionValues;
};