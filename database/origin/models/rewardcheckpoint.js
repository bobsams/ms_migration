'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RewardCheckpoint extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  RewardCheckpoint.init({
    step: DataTypes.INTEGER,
    stars_required: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'RewardCheckpoint',
  });
  return RewardCheckpoint;
};