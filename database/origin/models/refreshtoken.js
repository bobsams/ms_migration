'use strict';
module.exports = (sequelize, DataTypes) => {
  const RefreshToken = sequelize.define('RefreshToken', {
    user_id: {
      allowNull: false,
      type:  DataTypes.INTEGER,
    },
    refresh_token: {
      allowNull: false,
      type:  DataTypes.STRING,
    },
    expires: {
      allowNull: false,
      type:  DataTypes.DATE
    },
  }, {});
  RefreshToken.associate = function(models) {
    // associations can be defined here
  };
  return RefreshToken;
};