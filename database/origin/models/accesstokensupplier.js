'use strict';
module.exports = (sequelize, DataTypes) => {
  const AccessTokenSupplier = sequelize.define('AccessTokenSupplier', {
    supplier_user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    access_token: {
      allowNull: false,
      type: DataTypes.STRING
    },
    expires: {
      allowNull: false,
      type: DataTypes.DATE
    },
    scopes: {
      allowNull: false,
      type: DataTypes.TEXT
    },
  }, {});
  AccessTokenSupplier.associate = function(models) {
    // associations can be defined here
  };
  return AccessTokenSupplier;
};