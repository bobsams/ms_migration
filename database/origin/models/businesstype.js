'use strict';
module.exports = (sequelize, DataTypes) => {
  const BusinessType = sequelize.define('BusinessType', {
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    name_en: {
      allowNull: false,
      type: DataTypes.STRING
    },
    icon: {
      allowNull: false,
      type: DataTypes.STRING
    }
  }, {});
  BusinessType.associate = function(models) {
    // associations can be defined here
  };
  return BusinessType;
};