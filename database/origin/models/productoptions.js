'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductOptions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ProductOptions.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      });
      ProductOptions.hasMany(models.ProductOptionValues, {
        foreignKey: 'product_option_id',
        as: 'optionValues'
      });
    }
  };
  ProductOptions.init({
    product_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProductOptions',
  });
  return ProductOptions;
};