'use strict';
module.exports = (sequelize, DataTypes) => {
  const District = sequelize.define('District', {
    city_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    sicepat_id: {
      allowNull: false,
      type: DataTypes.STRING
    },
    sap_id: {
      allowNull: false,
      type: DataTypes.STRING
    },
    jne_id: {
      allowNull: false,
      type: DataTypes.STRING
    },
    zone: {
      type: DataTypes.STRING
    }
  }, {});
  District.associate = function(models) {
    // associations can be defined here
  };
  return District;
};