'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierCourier = sequelize.define('SupplierCourier', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    courier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    courier_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    service: {
      allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  SupplierCourier.associate = function(models) {
    // associations can be defined here
  };
  return SupplierCourier;
};