'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderTimeline = sequelize.define('OrderTimeline', {
    order_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    order: {
      allowNull: false,
      type: DataTypes.DATE
    },
    packaging: {
      type: DataTypes.DATE
    },
    ready_to_pickup: {
      type: DataTypes.DATE
    },
    send: {
      type: DataTypes.DATE
    },
    finish: {
      type: DataTypes.DATE
    },
  }, {});
  OrderTimeline.associate = function(models) {
    // associations can be defined here
    OrderTimeline.belongsTo(models.Order, {
      foreignKey: 'order_id',
      as: 'timeline'
    });
  };
  return OrderTimeline;
};