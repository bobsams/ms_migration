'use strict';
module.exports = (sequelize, DataTypes) => {
  const TokoCourierService = sequelize.define('TokoCourierService', {
    toko_id: DataTypes.INTEGER,
    courier_id: DataTypes.INTEGER,
    courier_code: DataTypes.STRING,
    service: DataTypes.STRING
  }, {});
  TokoCourierService.associate = function(models) {
    // associations can be defined here
  };
  return TokoCourierService;
};