'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserKYC = sequelize.define('UserKYC', {
    user_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    fullname: {
      allowNull: false,
      type: DataTypes.STRING
    },
    date_of_birth: {
      type: DataTypes.STRING
    },
    gender: {
      type: DataTypes.STRING
    },
    identity_number: {
      allowNull: false,
      type: DataTypes.STRING
    },
    identity_type: {
      allowNull: false,
      type: DataTypes.STRING
    },
    identity_image: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    selfie_image: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING
    },
    request_date: {
      allowNull: false,
      type: DataTypes.DATE
    },
    approved_date: {
      type: DataTypes.DATE
    },
    rejected_date: {
      type: DataTypes.DATE
    },
    rejected_reason: {
      type: DataTypes.TEXT
    },
    last_action_by: {
      type: DataTypes.STRING
    }
  }, {});
  UserKYC.associate = function(models) {
    // associations can be defined here
    UserKYC.belongsTo(models.User, {
      foreignKey: 'user_id',
      as: 'user'
    })
  };
  return UserKYC;
};