'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RewardTransaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      RewardTransaction.belongsTo(models.User, {
        foreignKey: 'user_id',
        as: 'user'
      })
      RewardTransaction.belongsTo(models.Transaction, {
        foreignKey: 'transaction_id',
        as: 'transaction'
      })
      RewardTransaction.hasMany(models.RewardOrder, {
        foreignKey: 'reward_trx_id',
        as: 'rewardOrder'
      })
    }
  };
  RewardTransaction.init({
    user_id: DataTypes.INTEGER,
    transaction_id: DataTypes.INTEGER,
    star_earned: DataTypes.INTEGER,
    star_potential: DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    valid_date: DataTypes.STRING, // null || date
    status: DataTypes.STRING // null | pending | valid
  }, {
    sequelize,
    modelName: 'RewardTransaction',
  });
  return RewardTransaction;
};