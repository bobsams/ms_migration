'use strict';
module.exports = (sequelize, DataTypes) => {
  const Portfolio = sequelize.define('Portfolio', {
    name: DataTypes.STRING,
    image: DataTypes.STRING,
    image_active: DataTypes.STRING,
    priority: DataTypes.INTEGER,
    status: DataTypes.BOOLEAN
  }, {});
  Portfolio.associate = function(models) {
    // associations can be defined here
    Portfolio.hasMany(models.SubPortfolio, {
      foreignKey: 'portfolio_id',
      as: 'subportfolio'
    });
  };
  return Portfolio;
};