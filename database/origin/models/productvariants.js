'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductVariants extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ProductVariants.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      });
      ProductVariants.belongsTo(models.ProductOptionValues, {
        foreignKey: 'product_option_value_id',
        as: 'option'
      });
      ProductVariants.belongsTo(models.ProductSKU, {
        foreignKey: 'product_sku_id',
        as: 'sku_detail'
      });
      ProductVariants.hasOne(models.SupplierCatalog, {
        foreignKey: 'product_variant_id',
        as: 'supplier_catalog_details'
      })
      ProductVariants.hasOne(models.TokoCatalog, {
        foreignKey: 'product_variant_id',
        as: 'toko_catalog_details'
      })
      ProductVariants.belongsTo(models.ProductSKU, {
        foreignKey: 'product_sku_id',
        as: 'sku_details'
      })
    }
  };
  ProductVariants.init({
    status: {
      type: DataTypes.BOOLEAN,
      // defaultValue: true
    },
    product_id: {
      // allowNull: false,
      type: DataTypes.INTEGER
    },
    product_option_value_id: DataTypes.INTEGER,
    product_sku_id: DataTypes.INTEGER,
    sku: DataTypes.STRING,
    weight: DataTypes.INTEGER,
    price: DataTypes.INTEGER,
    capital_price: DataTypes.INTEGER,
    stock: DataTypes.INTEGER,
    minimum_stock: DataTypes.INTEGER,
    code: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'ProductVariants',
  });
  return ProductVariants;
};