'use strict';
module.exports = (sequelize, DataTypes) => {
  const Announcement = sequelize.define('Announcement', {
    title: DataTypes.STRING,
    content: DataTypes.TEXT,
    image: DataTypes.STRING,
    color: DataTypes.STRING
  }, {});
  Announcement.associate = function(models) {
    // associations can be defined here
  };
  return Announcement;
};