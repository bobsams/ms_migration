'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};


const connectionOption = {
  host: 'payok-staging-db.chlatz5xa3ra.ap-southeast-1.rds.amazonaws.com',
  dialect: 'postgres', 
  pool: {
    max: 100,
    min: 0,
    // @note https://github.com/sequelize/sequelize/issues/8133#issuecomment-359993057
    acquire: 100 * 1000
  },
  // logging: false
}

let sequelize;
sequelize = new Sequelize(
  'tokook',
  'staginguseradmin',
  'slowDown!13ruh...',
  connectionOption
);

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection to ORIGIN_DB has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });


fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
