'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierKYC = sequelize.define('SupplierKYC', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    type: {
      allowNull: false,
      type: DataTypes.STRING
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    ktp_number: {
      type: DataTypes.STRING
    },
    ktp_image: {
      type: DataTypes.STRING
    },
    npwp_number: {
      type: DataTypes.STRING
    },
    npwp_image: {
      allowNull: false,
      type: DataTypes.STRING
    },
    ktp_selfie_image: {
      type: DataTypes.STRING
    },
    nib_number: {
      type: DataTypes.STRING
    },
    nib_image: {
      type: DataTypes.STRING
    },
    rekening_book_image: {
      allowNull: false,
      type: DataTypes.STRING
    },
    status: {
      allowNull: false,
      type: DataTypes.STRING
    },
    request_date: {
      allowNull: false,
      type: DataTypes.DATE
    },
    approved_date: {
      type: DataTypes.DATE
    },
    rejected_date: {
      type: DataTypes.DATE
    },
    rejected_reason: {
      type: DataTypes.TEXT
    },
    last_action_by: {
      type: DataTypes.STRING
    },
  }, {});
  SupplierKYC.associate = function(models) {
    // associations can be defined here
    SupplierKYC.belongsTo(models.Supplier, {
      foreignKey: 'supplier_id',
      as: 'supplier'
    })
  };
  return SupplierKYC;
};