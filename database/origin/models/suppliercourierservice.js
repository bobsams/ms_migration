'use strict';
module.exports = (sequelize, DataTypes) => {
  const SupplierCourierService = sequelize.define('SupplierCourierService', {
    supplier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    courier_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    courier_code: {
      allowNull: false,
      type: DataTypes.STRING
    },
    service: {
      allowNull: false,
      type: DataTypes.STRING
    },
  }, {});
  SupplierCourierService.associate = function(models) {
    // associations can be defined here
  };
  return SupplierCourierService;
};