'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class BannerTutorial extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  BannerTutorial.init({
    title: DataTypes.STRING,
    content_url: DataTypes.STRING,
    image_url: DataTypes.STRING,
    status: DataTypes.BOOLEAN,
    order: DataTypes.INTEGER,
    type: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'BannerTutorial',
  });
  return BannerTutorial;
};