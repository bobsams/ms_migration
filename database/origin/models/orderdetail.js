'use strict';
module.exports = (sequelize, DataTypes) => {
  const OrderDetail = sequelize.define('OrderDetail', {
    order_id: {
      type: DataTypes.INTEGER
    },
    product_id: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    price: {
      allowNull: false,
      type: DataTypes.BIGINT
    },
    earning:{
      type: DataTypes.INTEGER
    },
    sku: {
      type: DataTypes.STRING
    },
    quantity: {
      allowNull: false,
      type: DataTypes.INTEGER
    },
    image: {
      allowNull: false,
      type: DataTypes.STRING
    },
    variant_name: {
      type: DataTypes.STRING
    },
    code: {
      type: DataTypes.STRING
    },
    supplier_price: {
      type: DataTypes.INTEGER
    },
    product_price: {
      type: DataTypes.INTEGER
    },
    markup_tokook: {
      type: DataTypes.INTEGER
    },
    marketplace_price: {
      type: DataTypes.INTEGER
    },
    commision_tokook: {
      type: DataTypes.DECIMAL
    },
  }, {});
  OrderDetail.associate = function (models) {
    // associations can be defined here
    OrderDetail.belongsTo(models.Order, {
      foreignKey: 'order_id',
      as: 'details'
    });
    OrderDetail.belongsTo(models.Product, {
      foreignKey: 'product_id',
      as: 'product'
    });
  };
  return OrderDetail;
};