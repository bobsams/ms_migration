'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProductStat extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      ProductStat.belongsTo(models.Product, {
        foreignKey: 'product_id',
        as: 'product'
      })
    }
  };
  ProductStat.init({
    product_id: DataTypes.INTEGER,
    min_price: DataTypes.INTEGER,
    max_price: DataTypes.INTEGER,
    view: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    share: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    }
  }, {
    sequelize,
    modelName: 'ProductStat',
    paranoid: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
  });
  return ProductStat;
};