'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class OptionType extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      OptionType.hasMany(models.OptionValue, {
        foreignKey: 'opt_type_id',
        as: 'values'
      })
    }
  };
  OptionType.init({
    name: DataTypes.STRING,
    master_data: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'OptionType',
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    paranoid: true
  });
  return OptionType;
};